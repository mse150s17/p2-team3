# Project 2

# README #

This README explains Team 3's project 2 repository for plotting data.

## What is this repository for? ##

This repository is designed to generate plots from tabulated raw data in text files, and create a file that can be opened in Excel.

### Spectra Data ###

The Spectra data is from experiments measuring intensity versus wavelength of the following gasses: H2, He, Hg, Ne, and H20  

### Bending Data

The Bending data is from experiments measuring the stress versus strain of the following materials: aluminum, tungsten, steel, glass, and ultem.
	
* Not all samples were tested to failure, which is why some look very different from others. 

## How do I get set up? ##

### Dependencies ###

* You cannot load multiple files at once, or plot multiple test samples (even if it's the same material). Each sample must be loaded separately.
* The element or material is not named in the graph itself, only in the file name and excel spreadsheet.

### Database configuration ###

* Bending: contains the complete tests for each material type
* Splitbendingfiles: contains the separated test samples for each material 

## How do I run the code?   

The current example code can be invoked with:

$ python spectraplot.py spectra/FILENAME

OR

$ python bendingplot.py bending/Splitbendingfiles/FILENAME

and it currently will read in the spectrum or bending data file into a numpy array, and verify it has done so as expected by printing it out.

## What should I expect? ##

### Spectra Data ###

* After you run the command found above a graph should pop up in the background. The graph should be titled "Spectra From Gas Discharges: Intensity vs. Wavelength". There are five different elements to choose from: H2, He, Hg, Ne, and H2O. Each looks slightly different, but consists of various peaks depending on the element.

### Bending Data  

* After you run the command found above a graph should pop up in the background. This graph should be titled "Bending Data: Stress vs. Strain". There are nine different samples for five materials: aluminum, tungsten, steel, glass, and ultem. The slopes of these graphs are positive because we added an absolute value function.

### Who do I talk to? ###

##### Repo owner or admin #####

* Eric Jankowski: ericjankowski@boisestate.edu 

##### Other community or team contact #####

* Kendra Noneman: kendranoneman@u.boisestate.edu
* John Page: johnpage@u.boisestate.edu
* Matt Lawson: matthewlawson@u.boisestate.edu
* Julie Pipkin: juliepipkin@u.boisestate.edu
* Kody King: kodyking@u.boisestate.edu
* Kiev Dixon: kievdixon@u.boisestate.edu
* Nate Farris: natefarris@u.boisestate.edu
