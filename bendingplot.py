import matplotlib.pyplot as plt
import numpy as np
import sys

#To run this scirpt $python bendingplot.py bending/Splitbendingfiles/FILENAME

FILE = sys.argv[1]
data = np.genfromtxt(FILE, delimiter=',', dtype=float) #Pulls data from individual test samples


a = [colomn[3] for colomn in data] #Selects only the values for stress
b = [colomn[7] for colomn in data] #Selects only the values for strain

#Values are all negative so when we plot stress vs strain graph we need them to be positive

a_abs = np.absolute(a)
b_abs = np.absolute(b)
plt.plot(b_abs,a_abs)
plt.title('Bending Data\nStress vs. Strain', color="green")
plt.grid(True)
plt.xlabel('Strain (extension %)')
plt.ylabel('Stress (MPa)')
plt.show()
