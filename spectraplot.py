import matplotlib.pyplot as plt
import numpy as np
import sys

#To run this file use $python spectraplot.py spectra/FILENAME

FILE = sys.argv[1]
plt.plotfile(FILE, delimiter=' ', cols=(0, 1), names=('Wavelength (nm)', 'Intensity (arb.)'))
plt.title('Spectra From Gas Discharges\nIntensity vs. Wavelength',color="green")
plt.grid(True) 
plt.show()
